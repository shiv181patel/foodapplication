﻿using UserAPI.Model;
using UserAPI.Repository;

namespace UserAPI.Service
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
 
        public List<User> GetAllUsers()
        {
           return _userRepository.GetAllUsers();
        }

        public User GetUserByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool RegisterUser(User user)
        {
           var userExist =_userRepository.GetUserByName(user.Name);
            if(userExist==null)
            {
               int userToAdd = _userRepository.RegisterUser(user);
                if(userToAdd==1)
                {
                    return true;
                }
                else return false;  
            }
            return false;
        }
        public bool EditUser(int id,User user)
        {
                int editStatus = _userRepository.EditUser(user);
                if (editStatus == 1)
                {
                    return true;
                }
                else return false;  
        }

        public User LogIn(LoginUser loginUser)
        {
            User user = _userRepository.LogIn(loginUser);
            if (user != null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }
    }
}
