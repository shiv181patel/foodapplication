﻿using UserAPI.Model;

namespace UserAPI.Service
{
    public interface IUserService
    {
        bool EditUser(int id,User user);
        List<User> GetAllUsers();
        User LogIn(LoginUser loginUser);
        bool RegisterUser(User user);   
    }
}