﻿using UserAPI.Model;
using UserAPI.Repository;

namespace UserAPI.Service
{
    public class FoodService : IFoodService
    {
        readonly IFoodRepository _foodRepository;
        public FoodService(IFoodRepository foodRepository)
        {
            _foodRepository = foodRepository;
        }
        public bool AddFood(Food food)
        {
            Food foodspresent = _foodRepository.GetFoodByName(food.FoodName);
            if (foodspresent != null)
            {
                return false;
            }
            else
            {
                _foodRepository.AddFood(food);
                return true;
            }

        }

        public bool DeleteFood(int id)
        {
            Food foodspresent = _foodRepository.GetFoodById(id);

            if (foodspresent != null)
            {
                _foodRepository.DeleteFoodById(id);
                return true;
            }
            else
            {
                return false;
            }

        }
        public List<Food> GetAllFood()
        {
            return _foodRepository.GetAllFood();
        }
        public Food GetFoodById(int id)
        {
            return _foodRepository.GetFoodById(id);

        }



    }
}
