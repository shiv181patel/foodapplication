﻿using UserAPI.Model;

namespace UserAPI.Service
{
    public interface IOrderService
    {
        bool CancelOrder(string id, string email);
        List<Order> GetAllOrder(string id);
        bool OrderFood(string id, string email);
    }
}
