﻿using UserAPI.Model;
using UserAPI.Repository;

namespace UserAPI.Service
{
    public class OrderService : IOrderService
    {
        readonly IOrderRepository _orderRepository;
        public OrderService(IOrderRepository orderRepository)
        {
            this._orderRepository = orderRepository;
        }
        public bool CancelOrder(string id, string email)
        {
            return _orderRepository.CancelOrder(id, email);
        }

        public List<Order> GetAllOrder(string id)
        {
            return _orderRepository.GetAllOrder(id);
        }

        public bool OrderFood(string id, string email)
        {
            return _orderRepository.OrderFood(id, email);
        }
    }
}
