﻿using UserAPI.Model;

namespace UserAPI.Service
{
    public interface IFoodService
    {
        bool AddFood(Food food);
        Food GetFoodById(int id);
        bool DeleteFood(int id);
        List<Food> GetAllFood();
    }
}
