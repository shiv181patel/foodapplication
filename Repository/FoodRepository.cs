﻿using UserAPI.Context;
using UserAPI.Model;

namespace UserAPI.Repository
{
    public class FoodRepository : IFoodRepository
    {
        readonly UserDbContext _userDbContext;
        public FoodRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }
        public void AddFood(Food food)
        {
            _userDbContext.Foods.Add(food);
            _userDbContext.SaveChanges();
        }
        public void DeleteFoodById(int id)
        {
            Food foodexits = _userDbContext.Foods.Where(u => u.FoodId == id).FirstOrDefault();
            _userDbContext.Foods.Remove(foodexits);
            _userDbContext.SaveChanges();
        }
        public List<Food> GetAllFood()
        {
           return _userDbContext.Foods.ToList();
        }
        public Food GetFoodById(int foodId)
        {
            return _userDbContext.Foods.Where(u => u.FoodId == foodId).FirstOrDefault();
        }

        public Food GetFoodByName(string? foodName)
        {
            return _userDbContext.Foods.Where(u => u.FoodName == foodName).FirstOrDefault();
        }
    }
}
