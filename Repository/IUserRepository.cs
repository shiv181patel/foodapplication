﻿using UserAPI.Model;

namespace UserAPI.Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        User GetUserByName(string? name);
        int RegisterUser(User user);
        User GetUserById(int id);
        int EditUser(User user);
        User LogIn (LoginUser loginUser);   
    }
}
