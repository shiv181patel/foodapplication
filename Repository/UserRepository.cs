﻿using UserAPI.Model;
using UserAPI.Context;
using System.Xml.Linq;

namespace UserAPI.Repository
{
    public class UserRepository : IUserRepository
    {
         readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext; 
        }

        public int EditUser(User user)
        {
            #region Trial
            //User userPresent = _userDbContext.Users.Where(u => u.Id == user.Id).FirstOrDefault();

            //user.Id = user.Id;
            //user.Name = user.Name;
            //user.PhoneNo = user.PhoneNo;    
            //user.Date = user.Date;  
            //user.Location = user.Location;  
            //user.Time = user.Time;  
            #endregion
            _userDbContext.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public List<User> GetAllUsers()
        {
            return _userDbContext.Users.ToList();
        }

        public User GetUserById(int id)
        {
            return _userDbContext.Users.Where(u => u.Id == id).FirstOrDefault();
        }

        public User GetUserByName(string? name)
        {
            return _userDbContext.Users.Where(u => u.Name == name).FirstOrDefault();
        }

        public int RegisterUser(User user)
        {
           _userDbContext.Users.Add(user);
            return _userDbContext.SaveChanges();
        }
        

        public User LogIn(LoginUser loginUser)
        {
           return _userDbContext.Users.Where(u => u.Name == loginUser.Email && u.Password == loginUser.Password).FirstOrDefault();    
        }
    }
}
