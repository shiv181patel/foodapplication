﻿using UserAPI.Context;
using UserAPI.Model;

namespace UserAPI.Repository
{
    public class OrderRepository : IOrderRepository
    {
        readonly UserDbContext _userDbContext;
        public OrderRepository(UserDbContext userDbContext)
        {
            this._userDbContext = userDbContext; 
        }
        public bool CancelOrder(string id, string email)
        {
            Order order = _userDbContext.Orders.Where(u=>u.OrderId == id).FirstOrDefault();
            order.CancelStatus = "Cancel Successful";
            _userDbContext.Entry(order).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            if (_userDbContext.SaveChanges()==1)
            {
                return true;
            }
            return false;
        }

        public List<Order> GetAllOrder(string id)
        {
           return _userDbContext.Orders.ToList();
        }

        public bool OrderFood(string id, string email)
        {
            Order order = new Order();
            DateTime dateTime = DateTime.Now;
            order.OrderId = dateTime.ToString("yyyyMMddhhmmss");
            order.Date= dateTime.ToString("dd/MM/yyyy");
            order.Time= dateTime.ToString("h:mm:ss tt");
            order.CancelStatus = "Not cancel";
            Food food=_userDbContext.Foods.Where(u => u.FoodId == int.Parse(id)).FirstOrDefault();
            order.FoodId = food.FoodId;
            order.FoodAmount =(float) food.FoodPrice;
            order.EmailId = email;
            _userDbContext.Orders.Add(order);
            _userDbContext.SaveChanges();
            return true;
        }
    }
}
