﻿using UserAPI.Model;

namespace UserAPI.Repository
{
    public interface IFoodRepository
    {
        void AddFood(Food food);
        void DeleteFoodById(int id);
        List<Food> GetAllFood();
        Food GetFoodById(int foodId);
        Food GetFoodByName(string? foodName);
    }
}
