﻿using UserAPI.Model;

namespace UserAPI.Repository
{
    public interface IOrderRepository
    {
        bool CancelOrder(string id, string email);
        List<Order> GetAllOrder(string id);
        bool OrderFood(string id, string email);
    }
}
