﻿using System.ComponentModel.DataAnnotations;

namespace UserAPI.Model
{
    public class LoginUser
    {
        public string Email { get; set; }    
        public string Password { get; set; }    
    }
}
