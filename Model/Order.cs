﻿namespace UserAPI.Model
{
    public class Order
    {
        public string OrderId { get; set; }
        public int FoodId { get; set; }
        public string EmailId { get; set; }
        public float FoodAmount { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string CancelStatus { get; set; }


    }
}
