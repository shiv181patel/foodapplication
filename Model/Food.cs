﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace UserAPI.Model
{
    public class Food
    {
        [Key]
        public int FoodId { get; set; }
        public string? FoodName { get; set; }
        public float? FoodPrice { get; set; }


    }
}
