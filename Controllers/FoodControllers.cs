﻿using Microsoft.AspNetCore.Mvc;
using UserAPI.Model;
using UserAPI.Service;

namespace UserAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodController : ControllerBase
    {
        readonly IFoodService _foodService;

        public FoodController (IFoodService foodService)
        {
            _foodService = foodService;
        }
        [Route("GelAllFood")]
        [HttpGet]
        public ActionResult GelAllFood()
        {
            List<Food> allfood = _foodService.GetAllFood();
            return Ok(allfood); 
        }

        [HttpPost]
        [Route("AddFood")]
        public ActionResult AddFood(Food food)
        {
            bool addFoodStatus = _foodService.AddFood(food);
            return Ok(addFoodStatus);
        }

        [HttpDelete]
        [Route("DeleteFood")]
        public ActionResult DeleteFood(int id)
        {
            bool deleteUserStatus = _foodService.DeleteFood(id);
            return Ok(deleteUserStatus);
        }
    }
     
}
