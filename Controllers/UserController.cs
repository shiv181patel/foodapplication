﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserAPI.Model;
using UserAPI.Service;

namespace UserAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpPost]
        [Route("register")]
        public ActionResult RegisterUser(User user)
        {
            bool registerUserStatus = _userService.RegisterUser(user);
            return Ok(registerUserStatus);
        }
        //[HttpPatch]
        [HttpPut]
        [Route("editUser")]
        public ActionResult EditUser(int id,User user)
        {
            bool EditUserStatus = _userService.EditUser(id,user);
            return Ok(EditUserStatus);
        }
        [HttpPost]
        [Route("login")]
        public ActionResult LogIn(LoginUser loginUser)
        {
            User user = _userService.LogIn(loginUser);
            return Ok(user);    
        }

    }
   
}
