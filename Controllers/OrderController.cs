﻿using Microsoft.AspNetCore.Mvc;
using UserAPI.Model;
using UserAPI.Service;

namespace UserAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        readonly IOrderService _orderService;

        public OrderController(IOrderService OrderService)
        {
            _orderService = OrderService;
        }
        [Route("GetOrders")]
        [HttpGet]
        public ActionResult GelAllOrder(string emailid)
        {
            List<Order> allorder = _orderService.GetAllOrder(emailid);
            return Ok(allorder);
        }
        [Route("Orderfood")]
        [HttpPost]
        public ActionResult OrderFood(string foodid,string email)
        {
            bool status = _orderService.OrderFood(foodid,email);
            return Ok(status);
        }
        [Route("CancelOrder")]
        [HttpPost]
        public ActionResult CancelOrder(string orderid)
        {
            bool status = _orderService.CancelOrder(orderid, "R");
            return Ok(status);
        }

    }
}
