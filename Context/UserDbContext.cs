﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using UserAPI.Model;

namespace UserAPI.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> Context):base(Context)
        {
            
        }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<UserInfo>().HasNoKey();
        //}
        public DbSet<User>Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        //public DbSet<Order> Orders { get; set; }
        public DbSet<Food> Foods { get; set; }

    }
}
